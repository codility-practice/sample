package com.zuitt.lesson2;

//lesson 2 CyclicRotation
public class CyclicRotation {

    public int[] solution(int[] A,int K){
        //Array size
        int arrSize=A.length;
        //terminate quickly if size is >=1
        if(arrSize<=1){
            return  A;
        }
        //1st looping for the number of shift needed
       for(int ctr=0;ctr<K;ctr++){
           //store last index value as it will be overwritten by the 2nd loop(shifting process)
            int lastIndex = A[arrSize-1];
            //shift right process
            for(int i = arrSize-1;i>0;i--){
                A[i] = A[i-1];
            }
            //after shifting, store the value of the last index to the front or first index
            A[0] = lastIndex;
        }
       return A;

    }
}
