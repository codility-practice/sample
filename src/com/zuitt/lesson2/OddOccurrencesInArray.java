package com.zuitt.lesson2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;

//lesson 2 OddOccurrencesInArray
public class OddOccurrencesInArray {

    public int solution(int[] A) {
        // write your code in Java SE 8

        /*
        55% score on codility
        Idea: check the occurrence of an int by removing it from the list,
         if it contains odd count(1 of then has no pair). return the value
         */

        //convert to list to use object built-in functions
        ArrayList<Integer> list = (ArrayList<Integer>) Arrays.stream(A).boxed().collect(Collectors.toList());

        boolean isAllRemove;
        //loop until all values are checked
        while (true){
            int curVal = list.get(0);
            list.remove(Integer.valueOf(curVal));
            int occurence=1;
            isAllRemove=false;
            //loop for counting of occurrence
            while (!isAllRemove){
                if(list.remove(Integer.valueOf(curVal))){
                    occurence++;
                }else{
                    //return the value without pair
                    if(occurence%2!=0){
                        return curVal;
                    }
                    isAllRemove=true;
                }

            }
        }
    }
}
