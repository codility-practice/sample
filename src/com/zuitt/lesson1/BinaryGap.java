package com.zuitt.lesson1;



public class BinaryGap {
    public int solution(int N) {
        // write your code in Java SE 8
        //get binary representation and remove all trailing zeroes for faster process
        String binaryRep = Integer.toBinaryString(N).replaceAll("0*$","");
        boolean isCount =false;
        int count=0;
        int maxCount = 0;
        //return 0 for empty representation including all '1' representation
        if(binaryRep.length()==0 || binaryRep.replaceAll("1","").length()==0){
            return 0;
        }
        for(int x = 0 ;x<binaryRep.length();x++){

            //stop counting of zeros, initialize values and stored the count.(change the count if higher than previous)
            if( count>0 && binaryRep.charAt(x)=='1'){
                isCount = false;
                maxCount = Math.max(count, maxCount);
                count=0;
            }
            //counting of zeroes
            if(isCount && binaryRep.charAt(x)=='0'){
                count++;
            }
            //mark to start the count of zeroes for the next loop
            if(binaryRep.charAt(x)=='1'){
                isCount=true;
            }
        }
        return maxCount;
    }
}
