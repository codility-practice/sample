package com.zuitt.lesson5;
//codility lesson 5 PassingCars

public class PassingCars {

    public int solutionPassingCars(int[] A) {
        // write your code in Java SE 8

        int toEast = 0;
        int pairCount=0;
        //looping inside A
        for(int x=0; x<A.length; x++){
            //add count of cars going to east
            if(A[x]==0){
                toEast++;
            }
            //add pairCount per cars going to east
            if(A[x]==1){
                pairCount +=  toEast;
            }
            //counter if a pair of cars exceed the limit
            if(pairCount>1000000000){
                return -1;
            }
        }
        //return pair count of not exceed the limit
        return pairCount;
    }
}
