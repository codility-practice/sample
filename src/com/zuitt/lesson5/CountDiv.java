package com.zuitt.lesson5;

public class CountDiv {

    public int solution(int A, int B, int K) {
        // write your code in Java SE 8
        /*
        Idea: get the divisible count of the range A-B by subtracting the count of A to B.
        add 1 if value of A is also divisible by K
         */
        int countDivA = A/K;
        int countDivB = B/K;
        int countDiv = countDivB-countDivA;
        if(A%K==0){
            countDiv++;
        }
        return countDiv;
    }
}
