package com.zuitt.lesson3;

import java.util.Arrays;

public class PermMissingElem {

    public int solution(int[] A) {
        // write your code in Java SE 8
        //empty array handler
        if(A.length==0){
            return 1;
        }
        //sort array from min to max
        Arrays.sort(A);
        //missing 1 handler
        if(A[0]!=1){
            return 1;
        }
        //special case for single element array
        if(A.length==1){
            return A[0]==1?2:1;
        }
        //loop to check if N != N+1
        for(int i = 1;i<A.length;i++) {
            if (A[i] != A[i-1]+1) {
                return A[i-1] + 1;
                }
            //handler for the last element(if no missing value)
            if(i==A.length-1){
                return A[i]+1;
            }
        }

        return 1;
    }
}
