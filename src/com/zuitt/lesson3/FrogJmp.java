package com.zuitt.lesson3;

//codility lesson 3 FromJmp
public class FrogJmp {

    public int solutionFrogJmp(int X,int Y,int D){
        // write your code in Java SE 8
        //get the number of jump needed
        int jumps = (Y-X)/D;
        //add 1 more jump for the remainder of the equation above
        if((Y-X)%D!=0){
            jumps++;
        }
        return jumps;
    }
}
