package com.zuitt.exercise5;

public class ThreeLetters {

        public String solution(int A, int B) {
            // write your code in Java SE 8
            StringBuilder result = new StringBuilder();

            if(A==0) {
                writer(B, 'b',result);
                return result.toString();
            }else if(B==0) {
                writer(A, 'a',result);
                return result.toString();

            }
            int ratio = A>=B?A/B:B/A;
            int remainder = A>=B?A%B:B%A;
            boolean isAHigherThanB = A>=B;
            int countOfLetters = ratio>1?2:1;
            boolean specialCase = ratio>2;
            if(specialCase) {
                remainder=isAHigherThanB?A-B:B-A;
                countOfLetters=1;
                ratio=1;
            }


            while(A>0 && B>0) {

                if(isAHigherThanB) {

                    A -=writer(Math.min(A, countOfLetters), 'a', result);
                    if(ratio==1 && remainder>0) {
                        A-=writer(Math.min(A, countOfLetters), 'a', result);
                        remainder-=countOfLetters;
                    }
                    B-=writer(Math.min(B, 1), 'b', result);
                }else {
                    B-=writer(Math.min(B, countOfLetters), 'b', result);
                    if(ratio==1 && remainder>0) {
                        B-=writer(Math.min(B, countOfLetters), 'b', result);
                        remainder-= Math.min(B, countOfLetters);
                    }
                    A -=writer(Math.min(A, 1), 'a', result);
                }

            }

            if(remainder>0 ) {
                if(isAHigherThanB) {
                    writer(remainder, 'a',result );
                }else {
                    writer(remainder, 'b',result );
                }
            }
            return result.toString();
        }

        private static int writer(int count,char letter,StringBuilder result) {
            int orgCount=count;
            while(count>0) {
                result.append(letter);
                count--;
            }
            return orgCount;
        }

    }

