package com.zuitt.exercise5;

public class ParityDegree {
    public int solution(int N) {
        // write your code in Java SE 8
        int initPower=0;
        int answer=0;
        //loop to try power of 2s starting from 0 until highest value
        while(true){
            if(N%Math.pow(2,initPower)==0){
                answer=initPower;
            }else{
                break;
            }
            initPower++;
        }
        return answer;
    }
}
