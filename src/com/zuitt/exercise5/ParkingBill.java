package com.zuitt.exercise5;

public class ParkingBill {

    public int solution(String E, String L)   {
        // write your code in Java SE 8
        /*
        idea: calculate the total duration in minutes. then compute using the given fees
         */
        //declarations
        String[] start = E.split ( ":" );
        String[] end = L.split ( ":" );
        int startHour = Integer.parseInt(start[0].trim());
        int endHour = Integer.parseInt(end[0].trim());
        int startMinute = Integer.parseInt(start[1].trim());
        int endMinute = Integer.parseInt(end[1].trim());
        int baseMinute=60;
        int totalHours;
        int payment;
        int entranceFee = 2;
        int totalMinutes;
        //identifier if duration in less than 1 hour
        if(startHour!=endHour){
            totalMinutes=baseMinute-startMinute+endMinute;
        }else {
            totalMinutes=endMinute-startMinute;
        }
        //for the partial minute of the start time consider as 1 hour
        startHour++;
        //looping to convert hours into minutes
        while(startHour<endHour){
            totalMinutes+=baseMinute;
            startHour++;
        }
        //convert back to hours to compute for the fees
        totalHours=totalMinutes/baseMinute;
        payment=+entranceFee;

        //add another hour for any extra minutes below 60
        if(totalMinutes%60!=0){

            totalHours++;
        }
        //payment computation
        if (totalHours>1){
            payment += (totalHours*4)-1;
        }else{
            payment +=3;
        }


        return payment;
    }
}
