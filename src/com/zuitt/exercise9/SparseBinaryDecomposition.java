package com.zuitt.exercise9;

public class SparseBinaryDecomposition {

    //40% score
    public int solution(int N) {
        // write your code in Java SE 8
        int i = 1;
        boolean isSparse1=false;
        boolean isSparse2=false;
        while(i<=(N/2)){
            String binRep1=Integer.toBinaryString(i);
            String binRep2=Integer.toBinaryString(N-i);
            System.out.println(binRep1);
            System.out.println(binRep2);
            isSparse1 = !binRep1.contains("11");
            isSparse2 = !binRep2.contains("11");

           if(isSparse1&&isSparse2){
               return i;
           }else{
               i++;
           }
        }
        return -1;
    }
}
